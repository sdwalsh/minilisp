grammar Lisp;

exp: list
   | atom
;

list:
    '('')'
    | '(' members ')'
;

members: (exp)+
;

atom: ID
    | NUM
;

NUM: ( '0' .. '9')+;
ID: ('a' .. 'z' | 'A' .. 'Z')+;
WHITESPACE : ( ' ' | '\r' '\n' | '\n' | '\t' ) {Skip();};
